;%include "io.inc"

extern printf


  section .data
  
    coefA    dq 0.0
    coefB    dq 0.0
    coefC    dq 0.0
    num1     dd -4.0
    num2     dd 2.0
    cero     dd 0.0
    negativo dd -1.0
    raiz     dd 0.0
    aux      dd 0.0
    suma     dd 0.0  
    resta     dd 0.0    
    dividendo dd 0.0
    x1       dq 0.0
    x2       dq 0.0        
    fmt1     db "La raiz N1 ES: %f", 10,0
    fmt2     db "La raiz N2 ES: %f", 10,0
    fmt3     db "No se pueden calcular las raices. El coeficiente A no puede ser 0.",10,0
    fmt4     db "Tiene Raices Imaginarias.",10,0
    section .text
    
    global CMAIN
    CMAIN:
    
    push ebp;
    mov ebp, esp; for correct debugging
 ;Se valida que el coeficiente A distinto de Cero   
            fld dword [ebp+8]
            ftst
            fstsw ax
            fwait
            sahf
           
            je finaliza1 ;En caso de que el coeficiente A sea 0, se informa por consola.
            fld dword [num1]    ; se ingresa -4
            fmulp               ; -4*a
            fld dword [ebp+16]  ; se ingresa el valor del coefC
            fmulp               ; -4*a*C
            fld dword [ebp+12]  ; se ingresa b
            fmul st0,st0        ; b*b
            faddp               ;b*b - 4*a*c
    
    ;Se valida que el resultado de b*b - 4*a*c sea mayor o igual a cero
            
            ftst
            fstsw ax
            fwait
            sahf
            
            jb finaliza2 ;En el caso de ser menor a cero se informa por consola que las raices son imaginarias  
           
    ;Se calcula la raiz con el resultado de b*b - 4*a*c     
            fsqrt
            fstp qword[raiz];se guarda el resultado en variable


            call calc_suma
            call calc_resta
            call imprimir_raices
            
            call salir
            
            ret
            
   
     
       
     imprimir_raices:
     fld qword[x1]
     push dword[x1+4]
     push dword[x1]
     push fmt1
     call printf
     add esp,12
     
     fld qword[x2]
     push dword[x2+4]
     push dword[x2]
     push fmt2
     call printf
     add esp,12
     
          
     ret
             
     calc_suma:
     fld dword [negativo]
     fld dword [ebp+12]
     fmulp
     fld qword [raiz]
     faddp
     fstp qword[suma]
     
     call divide1
     ret 
     
     divide1: 
     
     fld dword [num2]
     fld dword [ebp+8]
     fmulp
     fld qword[suma]
     fdiv st0,st1 
     fstp qword[x1]
     
     ret
          
          
     calc_resta:
     fld dword [negativo]
     fld dword [ebp+12]
     fmulp
     fld qword [raiz]
     fsubp
     fstp qword[resta] 
     call divide2
     ret
     
     divide2:    
     fld dword [num2]
     fld dword [ebp+8]
     fmulp
     fld qword[resta]
     fdiv st0,st1 
     fstp qword[x2]
     
     ret 
  
     finaliza1: 
     push fmt3
     call printf
     add esp,4

     call salir
     ret
     
     finaliza2: 
     push fmt4
     call printf
     add esp,4
     
     call salir
     ret
     
     
     salir:
     
     pop edx
     mov esp,ebp
     pop ebp
     
     ret 
