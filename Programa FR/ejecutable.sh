#!/bin/bash

echo Script para ejecutar la Formula Resolvente
echo
sleep 1s

cd /home/lapassesungs/Escritorio/TP1

echo Compilando programas 
echo
sleep 1s

nasm -f elf32 FormResolvente.s -o resolvente.o

sleep 1s

gcc -m32 -o ejecutable resolvente.o Form.c


echo Fin de compilacion
echo
sleep 1s

echo ejecución de programas
echo
echo ----------------------
echo Formula Resolvente
echo ----------------------
sleep 1s
echo
./ejecutable

sleep 1s
echo
echo ----------------------
echo Raices Calculadas
echo ----------------------
sleep 1s
