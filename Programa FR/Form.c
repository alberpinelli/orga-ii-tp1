#include <stdio.h>

extern void CMAIN( float a, float b, float c );

void main()
{
    float a;
    float b;
    float c;

    printf("Formula Resolvente\n");

    printf("Ingrese el valor para \"a\":\n");
    scanf("%f", &a);
    printf("Ingrese el valor para \"b\":\n");
    scanf("%f", &b);
    printf("Ingrese el valor para \"c\":\n");
    scanf("%f", &c);

    CMAIN( a, b, c);
}