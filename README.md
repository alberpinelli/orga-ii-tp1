                                      **UNIVERSIDAD NACIONAL GENERAL SARMIENTO**
                                          **ORGANIZACIÓN DEL COMPUTADOR II**

                                                **TRABAJO PRÁCTICO N°1**



1ER CUATRIMETRE AÑO 2021
Alumno: Pinelli Raul Alberto





**Ejercicio 1:**

Se realizó un programa en C que a través de la formula resolvente calcula las raíces de una funcion cuadrática. 

Para hacer uso de la formula, debemos ejecutar el script "ejecutable.sh". El mismo, en primera instancia compilará los programas y luego procederá a su ejecución.

![Compilación de los programas](./EjecuciónScript.jpg)

**Ejercicio 4 - FPU (Obligatorio)**

-Se realizó el ejercicio de FPU en Assembler, donde se calcula la suma de los valores de un arreglo y se imprime por consola.

-Podrán observar el codigo en el archivo **"SumaArreglo.s"** que se encuentra en el repositorio.



**Ejercicio 4 (Obligatorio)**


- Para el desarrollo del ejercicio, primero calculamos algunos datos que nos serviran como herramientas para responder lo solicitado.


- **Direcciones Virtuales:**

El enunciado nos informa que se utiliza un sistema de paginación con direcciones virtuales de 32 bits. Según lo indicado, podemos calcular los GB de memoria virtual.

32 bits = 30^2 x 2^2 = 1 GB x 4 = **4GB**

- **Tamaño de Página**

Como se indica que los frames tienen un tamaño de 4 MB, podemos decir que el tamaño de páginas también será de 4 MB.

Calculamos la cantidad de entradas que tenemos para 4 MB de páginas. 

4 MB = 4 x 1 MB = 2^2 x 2^20 = 2^22 -> **Contamos con 2^22 entradas.**


**_ITEM A_**

La tabla de páginas de un sistema de paginación de un solo nivel cuenta con **2^10 = 1024 entradas.**

En este caso, la cantidad de entradas la obtenemos a través del número de páginas que podemos tener con direcciones virtuales de 32 bits.

Es decir:  _[10 bits para #número de páginas ,22 bits para offset]_ Total de 32 bits.

**_ITEM B_**

Si se utiliza un sistema de tabla de paginación invertido, solo contamos con la entradas válidas. Por lo cual, procedemos a calcular la cantidad de entradas válidas dentro de los 3 GB de la memoria principal.

_RAM 3 GB = 3072 MB / 4 MB tamaño de página =_ **768 Entradas válidas.**


**_ITEM C_**

Presentamos la siguiente propuesta de un esquema de tablas multinivel de dos niveles.

Tenemos [P1] que corresponden a los primeros 5 bits de los 32 bits de direccion virtual. En él se encontraran las entradas a los directorios de tablas de páginas. Cuenta con 2^5 entradas.

A su vez, pra cada entrada de [P1], existen 2^5 entradas en [P2] correspondientes a las entradas de página.

Por último, contamos con [d] que son los 22 bits de offset. 

Por lo tanto el esquema quedaría de la siguiente manera --> _[P1][P2][d] -> [5 bits][5 bits][22 bits]_


**Ejercicio 6 (Obligatorio)**

_**Contamos con los siguientes registros de segmento para un proceso.**_

CS -> base address: 10000, limit: 25000

DS -> base address: 5000, limit: 4000

SS -> base address: 50, limit: 3500


_**Por otro lado, el proceso lee las siguientes direcciones lógicas:**_

A. La dirección 1 para el segmento de datos.

B. La dirección 520 para el segmento de código.

C. La dirección 350 para el segmento de stack.

D. La dirección 4000 para el segmento de stack.


_**Calculamos la dirección física en cada item**_

_**ITEM A:**_ Dirección 5001

_**ITEM B:**_ Dirección 10520

_**ITEM C:**_ Dirección 400

_**ITEM D:**_ _Dirección 4050 -> se produce TRAP debido a que supera el límite 3550_


**Ejercicio 7 (Obligatorio)**



**TLB INICIAL**
| Página | Frame |Tiempo|       
| ------ | ------ | ------ |   
| 0 | 3 | 0 |                  
| 3 | 2 | 1 |

**Tabla de Página INICIAL**
| Página | Frame |Valid|Tiempo| 
| ------ | ------ | ------ | ------ |
| 0 | 3 | V | 0 |
| 1 | - | I |  |
| 2 | - | I |4  |
| 3 | 2 | V | 1 |
| 4 | 0 | V | 2 |
| 5 | 1| V | 3 |


_**Se requieren de las siguientes páginas**_

**A.** Pagina 0, Pagina 2, Pagina 0, Pagina 4, Pagina 5

**B.** Pagina 2, Pagina 1, Pagina 0, Pagina 3, Pagina 4

**Memoria Princial**
| Frame 0 | Frame 1 | Frame 2 | Frame 3 | Frame 4 | Frame 5 | Frame 3 | Frame 0 |

**Backing Store**
| - | - | Página 1 | - | - | Página 2 | - | - |


**Aclaraciones**
● Se tiene un esquema de paginación con 6 páginas , 4 frames, una TLB con dos entradas
y un backing store ilimitado.La columna tiempo indica el orden de llegada, donde el
valor 0 es el más antiguo

● Para decidir qué página se reemplaza en cada momento se utiliza la política de
reemplazo FIFO (first-in , first-out).

● Siempre que se utiliza una entrada de la tabla de páginas, se actualiza la TLB.

● No se contabilizan los tiempos de escritura en este ejercicio.

● Los tiempos de acceso son los siguientes:

○ TLB -> 1 rafaga

○ Tabla de paginas -> 2 rafagas

○ Backing Store -> 10 rafagas.

_**PARA VERIFICAR LOS RESULTADOS DEL EJERCICIO, REVISAR EL PDF ADJUNTO EN EL REPOSITORIO ("EJERCICIO 7 ITEM A.PDF" y "EJERCICIO 7 ITEM B.PDF")**_
