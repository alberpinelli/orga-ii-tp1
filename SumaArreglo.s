%include "io.inc"

extern printf

section .data
    
    arreglo         dq 1.0,2.1,2.2,0.8,7.8
    largo           equ ($ - arreglo)/8
    suma            dq 0
    mensaje_Suma    db "La Suma del arreglo es: %f",10,0
    mensaje_largo   db "El largo del arreglo es: %d",10,0

section .text

global main
main:
    mov ebp, esp; for correct debugging

;imprimo el largo del arreglo
    push largo
    push mensaje_largo
    call printf
    add esp, 8
    
;cargo valores en registros        
    mov ebx, arreglo
    mov ecx, largo
    xor edx, edx
    
;agrego suma a la pila
    fld qword[suma]
       
calculaSuma:

    fld qword[ebx+edx*8]
    faddp
    inc edx
    
    cmp ecx, edx
    jg calculaSuma

    fstp qword[suma]    
    
    push dword[suma+4]
    push dword[suma]
    push mensaje_Suma
    call printf
    add esp, 12
    
    ret


